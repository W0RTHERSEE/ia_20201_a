﻿namespace RepasoPOO
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBnombre = new System.Windows.Forms.TextBox();
            this.Nedad = new System.Windows.Forms.NumericUpDown();
            this.RBmasculino = new System.Windows.Forms.RadioButton();
            this.RBfemenino = new System.Windows.Forms.RadioButton();
            this.BTaceptar = new System.Windows.Forms.Button();
            this.BTgetdatos = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Nedad)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "label3";
            // 
            // TBnombre
            // 
            this.TBnombre.Location = new System.Drawing.Point(98, 17);
            this.TBnombre.Name = "TBnombre";
            this.TBnombre.Size = new System.Drawing.Size(100, 20);
            this.TBnombre.TabIndex = 3;
            // 
            // Nedad
            // 
            this.Nedad.Location = new System.Drawing.Point(98, 59);
            this.Nedad.Name = "Nedad";
            this.Nedad.Size = new System.Drawing.Size(44, 20);
            this.Nedad.TabIndex = 4;
            // 
            // RBmasculino
            // 
            this.RBmasculino.AccessibleName = "";
            this.RBmasculino.AutoSize = true;
            this.RBmasculino.Location = new System.Drawing.Point(98, 94);
            this.RBmasculino.Name = "RBmasculino";
            this.RBmasculino.Size = new System.Drawing.Size(73, 17);
            this.RBmasculino.TabIndex = 5;
            this.RBmasculino.TabStop = true;
            this.RBmasculino.Text = "Masculino";
            this.RBmasculino.UseVisualStyleBackColor = true;
            // 
            // RBfemenino
            // 
            this.RBfemenino.AutoSize = true;
            this.RBfemenino.Location = new System.Drawing.Point(202, 94);
            this.RBfemenino.Name = "RBfemenino";
            this.RBfemenino.Size = new System.Drawing.Size(71, 17);
            this.RBfemenino.TabIndex = 6;
            this.RBfemenino.TabStop = true;
            this.RBfemenino.Text = "Femenino";
            this.RBfemenino.UseVisualStyleBackColor = true;
            // 
            // BTaceptar
            // 
            this.BTaceptar.Location = new System.Drawing.Point(198, 131);
            this.BTaceptar.Name = "BTaceptar";
            this.BTaceptar.Size = new System.Drawing.Size(75, 23);
            this.BTaceptar.TabIndex = 7;
            this.BTaceptar.Text = "Aceptar";
            this.BTaceptar.UseVisualStyleBackColor = true;
            this.BTaceptar.Click += new System.EventHandler(this.BTaceptar_Click);
            // 
            // BTgetdatos
            // 
            this.BTgetdatos.Location = new System.Drawing.Point(346, 131);
            this.BTgetdatos.Name = "BTgetdatos";
            this.BTgetdatos.Size = new System.Drawing.Size(75, 23);
            this.BTgetdatos.TabIndex = 8;
            this.BTgetdatos.Text = "Obtener datos";
            this.BTgetdatos.UseVisualStyleBackColor = true;
            this.BTgetdatos.Click += new System.EventHandler(this.BTgetdatos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BTgetdatos);
            this.Controls.Add(this.BTaceptar);
            this.Controls.Add(this.RBfemenino);
            this.Controls.Add(this.RBmasculino);
            this.Controls.Add(this.Nedad);
            this.Controls.Add(this.TBnombre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Nedad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBnombre;
        private System.Windows.Forms.NumericUpDown Nedad;
        private System.Windows.Forms.RadioButton RBmasculino;
        private System.Windows.Forms.RadioButton RBfemenino;
        private System.Windows.Forms.Button BTaceptar;
        private System.Windows.Forms.Button BTgetdatos;
    }
}

