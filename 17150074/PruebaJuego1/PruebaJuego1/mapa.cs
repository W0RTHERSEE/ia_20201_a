﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace PruebaJuego1
{
    class mapa
    {
        Texture2D TileSet;
        Dictionary <int, Tile> TilesFuente;
        List<CapaTiles> CapasTiles;

        public mapa (Texture2D TileSet)
        {
            this.TileSet = TileSet;
        }

        public void  CrearCalabozoDePrueba()
        {
            CapasTiles = new List<CapaTiles>();

            CapasTiles.Add(new CapaTiles(TilesFuente, 15, 15));
            CapasTiles[0].CrearRectanguloTilesSuelo(TipoTile.PIEDRA, new Rectangle(0, 0, 15, 15));
        }

        private void InicializarTileFuente()
        {
            TilesFuente = new Dictionary<int, Tile>();

            TilesFuente.Add(TipoTile.VACIO, new Tile(TileSet, new Rectangle(224, 320, 32, 32)));
            TilesFuente.Add(TipoTile.SUELO | TipoTile.PIEDRA + 1, new Tile(TileSet, new Rectangle(256, 160, 32, 32)));
            TilesFuente.Add(TipoTile.SUELO | TipoTile.PIEDRA + 2, new Tile(TileSet, new Rectangle(288, 160, 32, 32)));
            TilesFuente.Add(TipoTile.SUELO | TipoTile.PIEDRA + 3, new Tile(TileSet, new Rectangle(256, 192, 32, 32)));
            TilesFuente.Add(TipoTile.SUELO | TipoTile.PIEDRA + 4, new Tile(TileSet, new Rectangle(288, 192, 32, 32)));

        }

    }
}
