﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PruebaJuego1
{

    public static class TipoTile
    {
        //tipo de tile
        public const int SUELO = 0x00000100;
        public const int PARED = 0x00000200;
        public const int VACIO = 0X00000400;

        //material de tile
        public const int PIEDRA = 0x00010000;
        public const int TIERRA = 0x00020000;

        //orientacion del tiel
        public const int DERECHA = 0x00001000;
        public const int IZQUIERDA = 0x00002000;
        public const int ARRIBA = 0x00004000;
        public const int ABAJO = 0x00005000;

       

    }

    public class Tile
    {
        Texture2D Grafico;
        Rectangle AreaDestino, AreaFuente;
        int cont_frame = 0;
        

        public Tile(Texture2D Grafico, Rectangle AreaDestino, Rectangle AreaFuente)
        {
            this.Grafico = Grafico;
            this.AreaDestino = AreaDestino;
            this.AreaFuente = AreaFuente;
        }

        public Tile(Texture2D Grafico, Rectangle AreaFuente)
        {
            this.Grafico = Grafico;
            this.AreaFuente = AreaFuente;
            AreaDestino = new Rectangle(0, 0, 32, 32);
        }

        public void Dibujate()
        {
            Juego.Dibujador.Draw(Grafico, AreaDestino, AreaFuente, Color.White);
        }

        public void Dibujate(int PosicionTileX, int PosicionTileY)
        {
            AreaDestino.X = PosicionTileX * AreaDestino.Width;
            AreaDestino.Y = PosicionTileY * AreaDestino.Height;

            Dibujate();
        }

        

        public void Actualizar(KeyboardState Teclado)
        {
            
        }
    }
}