﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PruebaJuego1
{
    class CapaTiles
    {
        Tile[,] Tiles;
        Dictionary<int, Tile> TilesFuente;

        public CapaTiles(Dictionary<int, Tile> TilesFuente, int TanTilesX, int TanTilesY)
        {
            this.TilesFuente = TilesFuente;
            Tiles = new Tile[TanTilesX, TanTilesY];
        }

        public void CrearRectanguloTilesSuelo(int Material, Rectangle Rectangulo)
        {
            for (int j=Rectangulo.Y; j<Rectangulo.Y + Rectangulo.Height; j++ )
            {
                for (int i = Rectangulo.X; j < Rectangulo.X + Rectangulo.Width; j++)
                {
                    Tiles[i, j] = TilesFuente[TipoTile.SUELO | Material | i];
                }
            }
        }

        public void Dibujate()
        {
            for(int j=0; j<Tiles.GetLength(1); j++ )
            {
                for(int i=0; i<Tiles.GetLength(0); i++)
                {
                    Tiles[i, j].Dibujate(i, j);
                }
            }
        }
    }
}
