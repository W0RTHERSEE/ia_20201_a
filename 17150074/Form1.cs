﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO
{
    public partial class Form1 : Form
    {
        Persona mi_persona;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void BTaceptar_Click(object sender, EventArgs e)
        {
            mi_persona = new Persona(TBnombre.Text, (int)Nedad.Value, RBmasculino.Checked);
        }

        private void BTgetdatos_Click(object sender, EventArgs e)
        {
            MessageBox.Show(mi_persona.GetDatos());
        }
    }
}
