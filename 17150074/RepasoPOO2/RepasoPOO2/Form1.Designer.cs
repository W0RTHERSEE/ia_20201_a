﻿namespace RepasoPOO2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudEdad = new System.Windows.Forms.NumericUpDown();
            this.rbHombre = new System.Windows.Forms.RadioButton();
            this.rbMujer = new System.Windows.Forms.RadioButton();
            this.btAceptar = new System.Windows.Forms.Button();
            this.RTdatos = new System.Windows.Forms.RichTextBox();
            this.BTinsertar = new System.Windows.Forms.Button();
            this.nudInsertar = new System.Windows.Forms.NumericUpDown();
            this.BTeliminar = new System.Windows.Forms.Button();
            this.nudEliminar = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudEdad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsertar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEliminar)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // textNombre
            // 
            this.textNombre.Location = new System.Drawing.Point(69, 6);
            this.textNombre.Name = "textNombre";
            this.textNombre.Size = new System.Drawing.Size(100, 20);
            this.textNombre.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Edad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Sexo";
            // 
            // nudEdad
            // 
            this.nudEdad.Location = new System.Drawing.Point(69, 32);
            this.nudEdad.Name = "nudEdad";
            this.nudEdad.Size = new System.Drawing.Size(62, 20);
            this.nudEdad.TabIndex = 6;
            // 
            // rbHombre
            // 
            this.rbHombre.AutoSize = true;
            this.rbHombre.Location = new System.Drawing.Point(69, 66);
            this.rbHombre.Name = "rbHombre";
            this.rbHombre.Size = new System.Drawing.Size(62, 17);
            this.rbHombre.TabIndex = 7;
            this.rbHombre.TabStop = true;
            this.rbHombre.Text = "Hombre";
            this.rbHombre.UseVisualStyleBackColor = true;
            // 
            // rbMujer
            // 
            this.rbMujer.AutoSize = true;
            this.rbMujer.Location = new System.Drawing.Point(137, 66);
            this.rbMujer.Name = "rbMujer";
            this.rbMujer.Size = new System.Drawing.Size(51, 17);
            this.rbMujer.TabIndex = 8;
            this.rbMujer.TabStop = true;
            this.rbMujer.Text = "Mujer";
            this.rbMujer.UseVisualStyleBackColor = true;
            // 
            // btAceptar
            // 
            this.btAceptar.Location = new System.Drawing.Point(17, 98);
            this.btAceptar.Name = "btAceptar";
            this.btAceptar.Size = new System.Drawing.Size(75, 23);
            this.btAceptar.TabIndex = 9;
            this.btAceptar.Text = "Aceptar";
            this.btAceptar.UseVisualStyleBackColor = true;
            this.btAceptar.Click += new System.EventHandler(this.btAceptar_Click);
            // 
            // RTdatos
            // 
            this.RTdatos.Location = new System.Drawing.Point(241, 6);
            this.RTdatos.Name = "RTdatos";
            this.RTdatos.Size = new System.Drawing.Size(271, 274);
            this.RTdatos.TabIndex = 11;
            this.RTdatos.Text = "";
            // 
            // BTinsertar
            // 
            this.BTinsertar.Location = new System.Drawing.Point(17, 148);
            this.BTinsertar.Name = "BTinsertar";
            this.BTinsertar.Size = new System.Drawing.Size(114, 23);
            this.BTinsertar.TabIndex = 12;
            this.BTinsertar.Text = "Insertar en Posicion";
            this.BTinsertar.UseVisualStyleBackColor = true;
            this.BTinsertar.Click += new System.EventHandler(this.BTinsertar_Click);
            // 
            // nudInsertar
            // 
            this.nudInsertar.Location = new System.Drawing.Point(137, 151);
            this.nudInsertar.Name = "nudInsertar";
            this.nudInsertar.Size = new System.Drawing.Size(62, 20);
            this.nudInsertar.TabIndex = 14;
            this.nudInsertar.ValueChanged += new System.EventHandler(this.nudPos_ValueChanged);
            // 
            // BTeliminar
            // 
            this.BTeliminar.Location = new System.Drawing.Point(17, 200);
            this.BTeliminar.Name = "BTeliminar";
            this.BTeliminar.Size = new System.Drawing.Size(114, 23);
            this.BTeliminar.TabIndex = 15;
            this.BTeliminar.Text = "Eliminar en posicion";
            this.BTeliminar.UseVisualStyleBackColor = true;
            this.BTeliminar.Click += new System.EventHandler(this.BTeliminar_Click);
            // 
            // nudEliminar
            // 
            this.nudEliminar.Location = new System.Drawing.Point(137, 203);
            this.nudEliminar.Name = "nudEliminar";
            this.nudEliminar.Size = new System.Drawing.Size(62, 20);
            this.nudEliminar.TabIndex = 16;
            this.nudEliminar.UseWaitCursor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RosyBrown;
            this.ClientSize = new System.Drawing.Size(524, 292);
            this.Controls.Add(this.nudEliminar);
            this.Controls.Add(this.BTeliminar);
            this.Controls.Add(this.nudInsertar);
            this.Controls.Add(this.BTinsertar);
            this.Controls.Add(this.RTdatos);
            this.Controls.Add(this.btAceptar);
            this.Controls.Add(this.rbMujer);
            this.Controls.Add(this.rbHombre);
            this.Controls.Add(this.nudEdad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textNombre);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudEdad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsertar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEliminar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudEdad;
        private System.Windows.Forms.RadioButton rbHombre;
        private System.Windows.Forms.RadioButton rbMujer;
        private System.Windows.Forms.Button btAceptar;
        private System.Windows.Forms.RichTextBox RTdatos;
        private System.Windows.Forms.Button BTinsertar;
        private System.Windows.Forms.NumericUpDown nudInsertar;
        private System.Windows.Forms.Button BTeliminar;
        private System.Windows.Forms.NumericUpDown nudEliminar;
    }
}

