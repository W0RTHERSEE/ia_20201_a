﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO2
{
    public partial class Form1 : Form
    {
        Persona mi_persona;
        Persona i_persona;
        Persona e_persona;

        ListaPersonas lista = new ListaPersonas();

        public Form1()
        {
            InitializeComponent();
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
           mi_persona = new Persona(textNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));

            lista.AgregarPersona(mi_persona);

            RTdatos.Text = lista.GetDatos();
            
        }

        private void bGetDatos_Click(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void BTinsertar_Click(object sender, EventArgs e)
        {
            i_persona = new Persona(textNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));
            lista.insertarPersona(i_persona, (int) nudInsertar.Value);
            RTdatos.Text = lista.GetDatos();
        }

        private void nudPos_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void BTeliminar_Click(object sender, EventArgs e)
        {
            e_persona = new Persona(textNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));
            lista.eliminarPersona(e_persona, (int)nudEliminar.Value);
            RTdatos.Text = lista.GetDatos();
        }
    }
}
