﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoPOO2
{
    class ListaPersonas
    {
        Persona Raiz = null;

        public void AgregarPersona(Persona persona_nueva)
        {
            if (Raiz == null)
            {
                Raiz = persona_nueva;
            }
            else
            {
                Persona aux = Raiz;

                while (aux.siguiente != null)
                {
                    aux = aux.siguiente;
                }

                aux.siguiente = persona_nueva;
            }
        }

        public String GetDatos()
        {
            String datos = "";
            Persona aux = Raiz;


            while (aux != null)
            {
                datos += aux.GetDatos() + "\n";
                aux = aux.siguiente;
            }
            return datos;
        }

        public void insertarPersona(Persona personainsertar, int pos)
        {
            
            if (pos==0)
            {
                personainsertar.siguiente = Raiz;
                Raiz = personainsertar;
                return;
            }
            Persona aux = Raiz;
            Persona aux_temporal;
            for (int i = 0; i < pos-1; i++)
            {
                if (aux.siguiente==null)
                {
                    break;
                }
                aux = aux.siguiente;
            }
            aux_temporal = aux.siguiente;
            aux.siguiente = personainsertar;
            personainsertar.siguiente = aux_temporal;
        }

        public void eliminarPersona(Persona personaeliminar, int pos_elim)
        {
            Persona aux=Raiz;
            if (pos_elim==0)
            {
                aux = Raiz.siguiente;
                Raiz = aux;
                return;
            }
            Persona aux2=null;
            for(int i=0; i<pos_elim-1; i++)
            {
                if(aux.siguiente==null)
                {
                    break;
                }
                aux = aux.siguiente;
            }
            personaeliminar = aux.siguiente;
            aux2 = personaeliminar.siguiente;
            aux.siguiente = aux2;
        }
    }
}
