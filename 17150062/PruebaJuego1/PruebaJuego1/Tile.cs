﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PruebaJuego1
{
    class Tile
    {
        Texture2D Grafico;
        //Vector2 Posicion;
        Rectangle AreaDestino;
        Rectangle AreaFuente;
        int cont_frame=0;

        public Tile(Texture2D Grafico, Rectangle AreaDestino, Rectangle AreaFuente)
        {
            this.Grafico = Grafico;
            //this.Posicion = Posicion;
            this.AreaDestino = AreaDestino;
            this.AreaFuente = AreaFuente;
        }

        public void Actualizar(KeyboardState Teclado)
        {
            AreaDestino.X++;
            if(cont_frame++ > 10)
            {
                AreaFuente.X += 145;
                if (AreaFuente.X > 870)
                    AreaFuente.X = 0;
                cont_frame = 0;
            }
        }

        public void Dibuja()
        {
            Juego.Dibujador.Draw(Grafico, AreaDestino, AreaFuente, Color.White);

        }
    }
}
