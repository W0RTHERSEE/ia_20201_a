﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoPOO
{
    class Persona
    {
        protected string nombre;
        protected int edad;
        protected bool EsMasculino;
        public Persona siguiente = null;

        public Persona(string nombre, int edad, bool EsMasculino)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.EsMasculino = EsMasculino;
        }

        public string getDatos()
        {
            string datos = "";
            datos += nombre + " ";
            datos += "Tiene " + edad + " años.\n";
            datos += EsMasculino ? " Es Hombre" : "Es Mujer";

            return datos;
        }
    }
}
