﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO
{
    public partial class Form1 : Form
    {
        Persona mi_persona;
        ListaPersona lista = new ListaPersona();

        public Form1()
        {
            InitializeComponent();
        }

        public void borrarCampo()
        {
            tbNombre.Clear();
            rbHombre.Checked = false;
            rbMujer.Checked = false;
            nudEdad.Value = 0;
            tbPos.Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bAceptar_Click(object sender, EventArgs e)
        {
            mi_persona = new Persona(tbNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));
            //MessageBox.Show("Datos guardados");
            lista.AgregarPersona(mi_persona);
            rtbLista.Text = lista.GetDatos();
            borrarCampo();
        }

        private void bgetDatos_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(mi_persona.getDatos());


        }

        private void rtbLista_TextChanged(object sender, EventArgs e)
        {

        }

        private void bInsertar_Click(object sender, EventArgs e)
        {
            mi_persona = new Persona(tbNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));
            lista.InsertarPersona(mi_persona, (Convert.ToInt32(this.tbPos.Text)));
            rtbLista.Text = lista.GetDatos();
            borrarCampo();
            //MessageBox.Show(ai_persona.GetDatos());
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            lista.EliminarPersona((Convert.ToInt32(this.tbdel.Text)));
            rtbLista.Text = lista.GetDatos();
        }
    }
}