﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO
{
    class ListaPersona
    {
        Persona Raiz = null;

        public void AgregarPersona(Persona persona_nueva)
        {
            if (Raiz == null)
            {
                Raiz = persona_nueva;
            }
            else
            {
                Persona auxiliar = Raiz;

                while (auxiliar.siguiente != null) 
                {
                    auxiliar = auxiliar.siguiente;
                } 

                auxiliar.siguiente = persona_nueva;
            }
        }
        public string GetDatos()
        {
            String Datos = "";
            //aqui recorremos la lista y le vamos concatenando
            //los datos de cada elemto de la lista 
            Persona auxiliar = Raiz;
            while(auxiliar != null)
            {
                Datos += auxiliar.getDatos() + "\n\n";
                auxiliar = auxiliar.siguiente;
            }
            
            return Datos;
        }

        public void InsertarPersona(Persona insertarP, int posicionI)
        {
            //En caso de querer posicion 0 el nuevo se convierte en la raiz, y la raiz se liga a este 
            if (posicionI == 0)
            {
                insertarP.siguiente = Raiz;
                Raiz = insertarP;
                return;
            }

            Persona auxiliar = Raiz;
            Persona auxiliar2;
            for (int i = 0; i < posicionI - 1; i++)
            {
                MessageBox.Show("recorre posicion");
                //en caso de posicion que no exista, rope ciclo e inserta al final
                if (auxiliar.siguiente == null)
                {
                    break;
                }
                auxiliar = auxiliar.siguiente;

            }
            auxiliar2 = auxiliar.siguiente;
            auxiliar.siguiente = insertarP;
            insertarP.siguiente = auxiliar2;
        }



        public void EliminarPersona (int posicionI)
        {
            Persona aux = Raiz;
            Persona aux2 = null;
            Persona aux3 = null;
            if (posicionI == 0)
            {
                aux = Raiz.siguiente;
                Raiz = aux;
                return;
            }

            for (int i = 0; i < posicionI - 1; i++)
            {
                MessageBox.Show("recorre posicion");
                //en caso de posicion que no exista, rope ciclo e inserta al final
                if (aux.siguiente == null)
                {
                    break;
                }
                aux = aux.siguiente;

            }
            aux2 = aux.siguiente;
            aux3 = aux2 .siguiente;
            aux.siguiente = aux3;

        }
    }
}

    
