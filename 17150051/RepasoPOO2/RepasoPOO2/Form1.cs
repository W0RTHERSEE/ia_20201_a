﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoPOO2
{
    public partial class Form1 : Form
    {

        public void borrarTexbox()
        {
            textNombre.Clear();
            rbHombre.Checked = false;
            rbMujer.Checked = false;
            nudEdad.Value = 0;
            nudPos.Value = 0;
        }
        Persona ai_persona,i_persona, nuevo;
        ListaPersonas lista = new ListaPersonas();
        public Form1()
        {
            InitializeComponent();
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            ai_persona = new Persona(textNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));
            lista.AgregarPersona(ai_persona);
            rtvDatos.Text = lista.getDatos();
            borrarTexbox();


        }

        private void btActualizar_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lista.eliminarPersona((int)nudElim.Value);
            rtvDatos.Text = lista.getDatos();
        }

        private void bGetDatos_Click(object sender, EventArgs e)
        {
            i_persona = new Persona(textNombre.Text, (int)nudEdad.Value, (rbHombre.Checked));
            lista.insertarPersona(i_persona, (int)nudPos.Value);
            rtvDatos.Text = lista.getDatos();
            borrarTexbox();
            //MessageBox.Show(ai_persona.GetDatos());

        }

        private void rtvDatos_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
