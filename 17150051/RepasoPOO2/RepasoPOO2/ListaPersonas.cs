﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace RepasoPOO2
{
    class ListaPersonas
    {
        Persona Raiz = null;
        
        public void AgregarPersona(Persona persona_nueva)
        {
            if(Raiz==null)
            {
                Raiz = persona_nueva;
            }
            else
            {
                Persona auxiliar = Raiz;
                while (auxiliar.siguiente != null)
                {
                  auxiliar = auxiliar.siguiente;
                }  

                auxiliar.siguiente = persona_nueva;
            }
        }
        
        public string getDatos()
        {
           
            String datos = "";
            
            Persona auxiliar = Raiz;
            while (auxiliar!= null)
            {
                datos += auxiliar.GetDatos() + "\n\n" ;
                auxiliar = auxiliar.siguiente;
            }
            
            return datos;
        }

        public void insertarPersona (Persona aInsertar, int posicion)
        {
            //En caso de querer posicion 0 el nuevo se convierte en la raiz, y la raiz se liga a este 
            if(posicion==0)
            {
                aInsertar.siguiente = Raiz;
                Raiz = aInsertar;
                return;
            }

            Persona auxiliar = Raiz;
            Persona auxiliar2;
            for (int i=0;i<posicion-1;i++) 
            {
                MessageBox.Show("recorre posicion");
                //en caso de posicion que no exista, rope ciclo e inserta al final
                if (auxiliar.siguiente == null)
                {
                    break;
                }
                auxiliar = auxiliar.siguiente;
                
            }
            auxiliar2 = auxiliar.siguiente;
            auxiliar.siguiente = aInsertar;
            aInsertar.siguiente = auxiliar2;

        }

        public void eliminarPersona(int posicion)
        {
            //En caso de querer posicion 0 el nuevo se convierte en la raiz, y la raiz se liga a este 
            Persona auxiliar = Raiz;
           
            if (posicion == 0)
            {
                auxiliar = Raiz.siguiente;
                Raiz = auxiliar;
                return;
            }
            Persona auxiliar2 = null;
            Persona auxiliar3 = null;
            for (int i=0;i<posicion-1; i++)
            {

             auxiliar = auxiliar.siguiente;

            }
            auxiliar2 = auxiliar.siguiente;
            auxiliar3 = auxiliar2.siguiente;
            auxiliar.siguiente = auxiliar3;
            

        }
    }
}
