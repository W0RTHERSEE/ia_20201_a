﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoPOO2
{
    class Persona
    {
            protected string Nombre;
            protected int Edad;
            protected bool EsMAsculino;
            public Persona siguiente = null;
        public Persona (string Nombre, int Edad, bool Esmasculino)
        {
            this.Nombre = Nombre;
            this.Edad = Edad;
            this.EsMAsculino = Esmasculino;

        }
        public string GetDatos()
        {
            string datos = "";
            datos += "Nombre: " + Nombre + "\n"; 
            datos += "Edad: " + Edad + "\n";
            datos += EsMAsculino ? "Hombre" : "Mujer";
            return datos;
        }
    }
}
