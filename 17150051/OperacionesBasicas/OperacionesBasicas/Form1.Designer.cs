﻿namespace OperacionesBasicas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.descomponerRGBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DescomponerRGB = new System.Windows.Forms.ToolStripMenuItem();
            this.escalaDeGrisesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componerRGBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vaciarMatrozAMapaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.espejoHorizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.espejoVerticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OFDImagen = new System.Windows.Forms.OpenFileDialog();
            this.PbImagen = new System.Windows.Forms.PictureBox();
            this.SFDImagen = new System.Windows.Forms.SaveFileDialog();
            this.etiquetadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.operacionesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.guardarToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.guardarToolStripMenuItem.Text = "Guardar";
            this.guardarToolStripMenuItem.Click += new System.EventHandler(this.guardarToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // operacionesToolStripMenuItem
            // 
            this.operacionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.descomponerRGBToolStripMenuItem,
            this.DescomponerRGB,
            this.escalaDeGrisesToolStripMenuItem,
            this.componerRGBToolStripMenuItem,
            this.vaciarMatrozAMapaToolStripMenuItem,
            this.espejoHorizontalToolStripMenuItem,
            this.espejoVerticalToolStripMenuItem,
            this.etiquetadoToolStripMenuItem});
            this.operacionesToolStripMenuItem.Name = "operacionesToolStripMenuItem";
            this.operacionesToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.operacionesToolStripMenuItem.Text = "Operaciones";
            // 
            // descomponerRGBToolStripMenuItem
            // 
            this.descomponerRGBToolStripMenuItem.Name = "descomponerRGBToolStripMenuItem";
            this.descomponerRGBToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.descomponerRGBToolStripMenuItem.Text = "VaciarMapaAMatriz";
            this.descomponerRGBToolStripMenuItem.Click += new System.EventHandler(this.descomponerRGBToolStripMenuItem_Click);
            // 
            // DescomponerRGB
            // 
            this.DescomponerRGB.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.DescomponerRGB.Name = "DescomponerRGB";
            this.DescomponerRGB.Size = new System.Drawing.Size(180, 22);
            this.DescomponerRGB.Text = "DescomponerRGB";
            this.DescomponerRGB.Click += new System.EventHandler(this.DescomponerRGB_Click);
            // 
            // escalaDeGrisesToolStripMenuItem
            // 
            this.escalaDeGrisesToolStripMenuItem.Name = "escalaDeGrisesToolStripMenuItem";
            this.escalaDeGrisesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.escalaDeGrisesToolStripMenuItem.Text = "Binarización";
            this.escalaDeGrisesToolStripMenuItem.Click += new System.EventHandler(this.escalaDeGrisesToolStripMenuItem_Click);
            // 
            // componerRGBToolStripMenuItem
            // 
            this.componerRGBToolStripMenuItem.Name = "componerRGBToolStripMenuItem";
            this.componerRGBToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.componerRGBToolStripMenuItem.Text = "ComponerRGB";
            this.componerRGBToolStripMenuItem.Click += new System.EventHandler(this.componerRGBToolStripMenuItem_Click);
            // 
            // vaciarMatrozAMapaToolStripMenuItem
            // 
            this.vaciarMatrozAMapaToolStripMenuItem.Name = "vaciarMatrozAMapaToolStripMenuItem";
            this.vaciarMatrozAMapaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.vaciarMatrozAMapaToolStripMenuItem.Text = "VaciarMatrozAMapa";
            this.vaciarMatrozAMapaToolStripMenuItem.Click += new System.EventHandler(this.vaciarMatrozAMapaToolStripMenuItem_Click);
            // 
            // espejoHorizontalToolStripMenuItem
            // 
            this.espejoHorizontalToolStripMenuItem.Name = "espejoHorizontalToolStripMenuItem";
            this.espejoHorizontalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.espejoHorizontalToolStripMenuItem.Text = "EspejoHorizontal";
            this.espejoHorizontalToolStripMenuItem.Click += new System.EventHandler(this.espejoHorizontalToolStripMenuItem_Click);
            // 
            // espejoVerticalToolStripMenuItem
            // 
            this.espejoVerticalToolStripMenuItem.Name = "espejoVerticalToolStripMenuItem";
            this.espejoVerticalToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.espejoVerticalToolStripMenuItem.Text = "EspejoVertical";
            this.espejoVerticalToolStripMenuItem.Click += new System.EventHandler(this.espejoVerticalToolStripMenuItem_Click);
            // 
            // OFDImagen
            // 
            this.OFDImagen.FileName = "openFileDialog1";
            this.OFDImagen.Filter = "Portable Network Graphics|*.png|Mapas de bits|*.bmp|Jpg|*.jpg";
            this.OFDImagen.FilterIndex = 3;
            // 
            // PbImagen
            // 
            this.PbImagen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PbImagen.Location = new System.Drawing.Point(12, 27);
            this.PbImagen.Name = "PbImagen";
            this.PbImagen.Size = new System.Drawing.Size(776, 411);
            this.PbImagen.TabIndex = 1;
            this.PbImagen.TabStop = false;
            this.PbImagen.Click += new System.EventHandler(this.PbImagen_Click);
            // 
            // etiquetadoToolStripMenuItem
            // 
            this.etiquetadoToolStripMenuItem.Name = "etiquetadoToolStripMenuItem";
            this.etiquetadoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.etiquetadoToolStripMenuItem.Text = "Etiquetado";
            this.etiquetadoToolStripMenuItem.Click += new System.EventHandler(this.etiquetadoToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PbImagen);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbImagen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem descomponerRGBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DescomponerRGB;
        private System.Windows.Forms.ToolStripMenuItem escalaDeGrisesToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog OFDImagen;
        private System.Windows.Forms.PictureBox PbImagen;
        private System.Windows.Forms.ToolStripMenuItem componerRGBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vaciarMatrozAMapaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem espejoHorizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem espejoVerticalToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog SFDImagen;
        private System.Windows.Forms.ToolStripMenuItem etiquetadoToolStripMenuItem;
    }
}

