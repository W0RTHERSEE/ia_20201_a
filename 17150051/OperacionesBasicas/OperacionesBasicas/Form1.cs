﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperacionesBasicas
{
    public partial class Form1 : Form
    {
        Binarizador operaciones;
        public Form1()
        {
            InitializeComponent();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
             
            if(OFDImagen.ShowDialog() == DialogResult.OK)
            {
                PbImagen.Image = new Bitmap(OFDImagen.FileName);
                operaciones = new Binarizador((Bitmap)PbImagen.Image);
            }

        }

        private void descomponerRGBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operaciones.VaciarMapasAMatriz();
        }

        private void DescomponerRGB_Click(object sender, EventArgs e)
        {
            operaciones.DescomponerRGB();
        }

        private void escalaDeGrisesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operaciones.EscalamientoDeGrises();
            operaciones.Binarizacion(64);
            
        }

        private void componerRGBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operaciones.ComponerRGB();
        }

        private void vaciarMatrozAMapaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PbImagen.Image = operaciones.VaciarMatrizAMapa();
        }

        private void espejoHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operaciones.espejoHorizontal();
        }

        private void espejoVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operaciones.espejoVertical();
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SFDImagen.ShowDialog() == DialogResult.OK)
                PbImagen.Image.Save(SFDImagen.FileName + ".jpg");
        }

        private void PbImagen_Click(object sender, EventArgs e)
        {

        }

        private void etiquetadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            operaciones.etiquetarComponentes();
        }
    }
}
