﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OperacionesBasicas
{
    class OperacionesBasicas
    {
        protected List<int[,]> segmentos;
        protected Bitmap MapaFuente;
        protected Bitmap MapaDestino;
        protected int Anchura;
        protected int Altura;
        protected int[,] Pixeles;
        protected int[,] R;
        protected int[,] G;
        protected int[,] B;
        int x;
        int y;

        public OperacionesBasicas(Bitmap MapaFuente)
        {
            this.MapaFuente = new Bitmap(MapaFuente);

           
        }
        public void VaciarMapasAMatriz()
        {
            Anchura = MapaFuente.Width;
            Altura = MapaFuente.Height;
            Pixeles = new int[Anchura, Altura];
            for (int j = 0; j < Altura; j++)
                for (int i = 0; i < Anchura; i++)
                {
                    Pixeles[i, j] = MapaFuente.GetPixel(i, j).ToArgb();

                }
        }

        public void DescomponerRGB()
        {
            R = new int[Anchura, Altura];
            G = new int[Anchura, Altura];
            B = new int[Anchura, Altura];


            for (int j = 0; j < Altura; j++)
                for (int i = 0; i < Anchura; i++)
                {
                    R[i, j] = (Pixeles[i, j] & 0x00ff0000) >> 16;
                    G[i, j] = (Pixeles[i, j] & 0x0000ff00) >> 8;
                    B[i, j] = (Pixeles[i, j] & 0x000000ff);

                }
        }

        public void ComponerRGB()
        {
            Pixeles = new int[Anchura, Altura];
            for(int j=0;j<Altura;j++)
                for(int i=0;i<Anchura; i++)
                {
                    Pixeles[i, j] = (255 << 24) | (R[i, j] << 16) | (G[i, j] << 8) | B[i, j];
                }

        }

        public Bitmap VaciarMatrizAMapa()
        {

            MapaDestino = new Bitmap(Anchura,Altura,System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            for(int j = 0; j<Altura;j++)
               for (int i=0;i<Anchura;i++)
                {
                    MapaDestino.SetPixel(i, j, Color.FromArgb(Pixeles[i, j]));

                }

                return MapaDestino;
        }

        public void prueba()
        {
            for(int j=10;j<100;j++)
                for(int i=10;i<100;i++)
                {
                    R[i,j] = 255;
                    G[i,j] = 0;
                    B[i,j] = 0;

                }

        }

        public void espejoHorizontal()
        {
           
            R = new int[Anchura, Altura];
            G = new int[Anchura, Altura];
            B = new int[Anchura, Altura];

        
            for (int j = 0; j < Altura; j++)
            {
                x = 0;
                for (int i = Anchura-1 ; i >=0 ; i--)
                {
                    R[i, j] = (Pixeles[x,j] & 0x00ff0000) >> 16;
                    G[i, j] = (Pixeles[x,j] & 0x0000ff00) >> 8;
                    B[i, j] = (Pixeles[x,j] & 0x000000ff);
                    x++;
                    
                }
            }
            
        }

        public void espejoVertical()
        {

            R = new int[Anchura, Altura];
            G = new int[Anchura, Altura];
            B = new int[Anchura, Altura];


            for (int i = 0; i < Anchura; i++)
            {
                x = 0;
                for (int j = Altura-1; j >=0 ;j--)
                {
                    
                    R[i, j] = (Pixeles[i, x] & 0x00ff0000) >> 16;
                    G[i, j] = (Pixeles[i, x] & 0x0000ff00) >> 8;
                    B[i, j] = (Pixeles[i, x] & 0x000000ff);
                    x++;

                }
            }

        }


 




    }
}
