﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OperacionesBasicas
{
    class Binarizador : OperacionesBasicas
    {
        public Binarizador(Bitmap MapaFuente) : base(MapaFuente)
        {

        }

        public void EscalamientoDeGrises()
        {
            int promedio;
            for (int j = 0; j < Altura; j++)
                for (int i = 0; i < Anchura; i++)
                {
                    promedio = (R[i, j] + G[i, j] + B[i, j]) / 3;
                    R[i, j] = promedio;
                    G[i, j] = promedio;
                    B[i, j] = promedio;
                }
        }

        public void Binarizacion(int Umbral)
        {
            for (int j = 0; j < Altura; j++)
                for (int i = 0; i < Anchura; i++)
                {
                    if (R[i, j] < Umbral)
                    {
                        R[i, j] = 0;
                        G[i, j] = 0;
                        B[i, j] = 0;
                    }
                    else
                    {
                        R[i, j] = 255;
                        G[i, j] = 255;
                        B[i, j] = 255;
                    }
                }
        }

        public void etiquetarComponentes()
        {
            string cadena = null;
            int cont = 0;
            int posicionArriba = 0;
            int posicionAbajo = 0;
            int posicionDerecha = 0;
            int posicionIzquierda = 0;
            int posicionActual = 0;
            int[,] NuevaMatriz = Pixeles;
            int[,] MatrizProcesada = Pixeles;
            int[,] NuevaMatrizProcesada = Pixeles;
             /////arriba hacia abajo primera vez
            for (int j = 0; j < Altura; j++)
            {
                for (int i = 0; i < Anchura; i++)
                {
                    //la condicion de -1 la tuvimos ue usar ya que cada punto blanco lo lee como un -1 y no como 0
                    if (Pixeles[i, j] != -1)
                    {
                        try
                        {
                            int x = j - 1;
                            posicionArriba = Pixeles[i, x];
                        }
                        catch { }



                        try
                        {
                            int x = j + 1;
                            posicionAbajo = Pixeles[i, x];
                        }
                        catch { }



                        try
                        {
                            int x = i + 1;
                            posicionDerecha = Pixeles[x, j];
                        }
                        catch { }



                        try
                        {
                            int x = i - 1;
                            posicionIzquierda = Pixeles[x, j];
                        }
                        catch { }


                        posicionActual = Pixeles[i, j];

                        if (posicionArriba != -1 && (posicionActual > posicionArriba)) MatrizProcesada[i, j] = posicionArriba;
                        if (posicionAbajo != -1 && (posicionActual > posicionAbajo)) MatrizProcesada[i, j] = posicionAbajo;
                        if (posicionDerecha != -1 && (posicionActual > posicionDerecha)) MatrizProcesada[i, j] = posicionDerecha;
                        if (posicionIzquierda != -1 && (posicionActual > posicionIzquierda)) MatrizProcesada[i, j] = posicionIzquierda;
                    }
                    

                   

                }
            }
            Pixeles = MatrizProcesada;


            ///arriba hacia abajo ciclo
            do
            {
                for (int j = 0; j < Altura; j++)
                {
                    for (int i = 0; i < Anchura; i++)
                    {
                        
                        if (Pixeles[i, j] != -1)
                        {
                            try
                            {
                                int x = j - 1;
                                posicionArriba = MatrizProcesada[i, x];///compara arriba
                            }
                            catch { }



                            try
                            {
                                int x = j + 1;
                                posicionAbajo = MatrizProcesada[i, x];///compara abajo
                            }
                            catch { }



                            try
                            {
                                int x = i + 1;
                                posicionDerecha = MatrizProcesada[x, j];///compara derecha
                            }
                            catch { }



                            try
                            {
                                int x = i - 1;
                                posicionIzquierda = MatrizProcesada[x, j];///compara izquierda
                            }
                            catch { }


                            posicionActual = MatrizProcesada[i, j];

                            if (posicionArriba != -1 && (posicionActual > posicionArriba)) NuevaMatrizProcesada[i, j] = posicionArriba;
                            if (posicionAbajo != -1 && (posicionActual > posicionAbajo)) NuevaMatrizProcesada[i, j] = posicionAbajo;
                            if (posicionDerecha != -1 && (posicionActual > posicionDerecha)) NuevaMatrizProcesada[i, j] = posicionDerecha;
                            if (posicionIzquierda != -1 && (posicionActual > posicionIzquierda)) NuevaMatrizProcesada[i, j] = posicionIzquierda;
                            ////comparar matriz anterior con nueva
                        }
                        
                    }
                }
                huboCambios(MatrizProcesada, NuevaMatrizProcesada);
                //final de for
                MatrizProcesada = NuevaMatrizProcesada;
                Pixeles = NuevaMatrizProcesada;

            }
            while (cont > 0);

            ////Abajo hacia arriba primera vez
            for (int j = Altura - 1; j >= 0; j--)
            {
                for (int i = Anchura - 1; i >= 0; i--)
                {
                    if (Pixeles[i, j] != -1)
                    {
                        try
                        {
                            int x = j - 1;
                            posicionArriba = Pixeles[i, x];
                        }
                        catch { }



                        try
                        {
                            int x = j + 1;
                            posicionAbajo = Pixeles[i, x];
                        }
                        catch { }



                        try
                        {
                            int x = i + 1;
                            posicionDerecha = Pixeles[x, j];
                        }
                        catch { }



                        try
                        {
                            int x = i - 1;
                            posicionIzquierda = Pixeles[x, j];
                        }
                        catch { }


                        posicionActual = Pixeles[i, j];

                        if (posicionArriba != -1 && (posicionActual > posicionArriba)) MatrizProcesada[i, j] = posicionArriba;
                        if (posicionAbajo != -1 && (posicionActual > posicionAbajo)) MatrizProcesada[i, j] = posicionAbajo;
                        if (posicionDerecha != -1 && (posicionActual > posicionDerecha)) MatrizProcesada[i, j] = posicionDerecha;
                        if (posicionIzquierda != -1 && (posicionActual > posicionIzquierda)) MatrizProcesada[i, j] = posicionIzquierda;
                    }
                }
            }
            Pixeles = MatrizProcesada;


            do
            {
                for (int j = Altura - 1; j >= 0; j--)
                {
                    for (int i = Anchura - 1; i >= 0; i--)
                    {

                        if (Pixeles[i, j] != -1)
                        {
                            try
                            {
                                int x = j - 1;
                                posicionArriba = MatrizProcesada[i, x];///compara arriba
                            }
                            catch { }



                            try
                            {
                                int x = j + 1;
                                posicionAbajo = MatrizProcesada[i, x];///compara abajo
                            }
                            catch { }



                            try
                            {
                                int x = i + 1;
                                posicionDerecha = MatrizProcesada[x, j];///compara derecha
                            }
                            catch { }



                            try
                            {
                                int x = i - 1;
                                posicionIzquierda = MatrizProcesada[x, j];///compara izquierda
                            }
                            catch { }


                            posicionActual = MatrizProcesada[i, j];

                            if (posicionArriba != -1 && (posicionActual > posicionArriba)) NuevaMatrizProcesada[i, j] = posicionArriba;
                            if (posicionAbajo != -1 && (posicionActual > posicionAbajo)) NuevaMatrizProcesada[i, j] = posicionAbajo;
                            if (posicionDerecha != -1 && (posicionActual > posicionDerecha)) NuevaMatrizProcesada[i, j] = posicionDerecha;
                            if (posicionIzquierda != -1 && (posicionActual > posicionIzquierda)) NuevaMatrizProcesada[i, j] = posicionIzquierda;
                            ////comparar matriz anterior con nueva
                        }

                    }
                }
                huboCambios(MatrizProcesada, NuevaMatrizProcesada);
                //final de for
                MatrizProcesada = NuevaMatrizProcesada;
                Pixeles = NuevaMatrizProcesada;

            }
            while (cont > 0);
        }
        //compara si hubo cambios en las matrices
        public int huboCambios(int[,] Pixeles, int[,] NuevaMatriz)
        {
            int cont = 0;
            for (int j = 0; j < Altura; j++)
            {
                for (int i = 0; i < Anchura; i++)
                {
                    if(Pixeles[i, j] != NuevaMatriz[i, j])
                    { 
                    cont++;
                    }

                }

            }
            return cont;
        }

        public void Segmentacion ()////////intentamos la segmentacion pero tenemos problemas con obtener tamaño de la matriz de cada segmento
        {                          // suponemos que para obtener el tamaño de la matriz, se necesita aplicar e lalgoritmos de A estrella que hablamos el ultimo dia
                                   // de clases presenciales.
            int cont = 0;
            int posicionArriba = 0;
            int posicionAbajo = 0;
            int posicionDerecha = 0;
            int posicionIzquierda = 0;
            int posicionActual = 0;
            int[,] nuevoSegmento = Pixeles;
            int[,] segmento = Pixeles;
            int[,] NuevaMatrizProcesada = Pixeles;
            segmentos = new List<int[,]>();///////////////////agrega matrices que contienen un segmento a la lista de segmentos
            for (int j = Altura - 1; j >= 0; j--)
            {
                for (int i = Anchura - 1; i >= 0; i--)
                {
                    if (Pixeles[i, j] != -1)
                    {
                        try
                        {
                            int x = j - 1;
                            nuevoSegmento[i, j] = Pixeles[i, j];
                            posicionArriba = Pixeles[i, x];///compara arriba
                            if(posicionArriba != -1)//////Comprueba si el vecino el vecino no es blanco, si es negro lo agrega a la matriz
                            {
                                nuevoSegmento[i, x] = Pixeles[i, x];
                                segmentos.Add(nuevoSegmento);
                                segmento = Pixeles;
                            }

                        }
                        catch { }



                        try
                        {
                            int x = j + 1;
                            nuevoSegmento[i, j] = Pixeles[i, j];
                            posicionAbajo = Pixeles[i, x];///compara abajo
                            if (posicionArriba != -1)//////Comprueba si el vecino el vecino no es blanco, si es negro lo agrega a la matriz
                            {
                                nuevoSegmento[i, x] = Pixeles[i, x];
                                segmentos.Add(nuevoSegmento);
                                segmento = Pixeles;
                            }
                        }
                        catch { }



                        try
                        {
                            int x = i + 1;
                            nuevoSegmento[i, j] = Pixeles[i, j];
                            posicionDerecha = Pixeles[x, j];///compara derecha
                            if (posicionArriba != -1)//////Comprueba si el vecino el vecino no es blanco, si es negro lo agrega a la matriz
                            {
                                nuevoSegmento[i, x] = Pixeles[i, x];
                                segmentos.Add(nuevoSegmento);
                                segmento = Pixeles;
                            }
                        }
                        catch { }



                        try
                        {
                            int x = i - 1;
                            nuevoSegmento[i, j] = Pixeles[i, j];
                            posicionIzquierda = Pixeles[x, j];///compara izquierda
                            if (posicionArriba != -1)//////Comprueba si el vecino el vecino no es blanco, si es negro lo agrega a la matriz
                            {
                                nuevoSegmento[i, x] = Pixeles[i, x];
                                segmentos.Add(nuevoSegmento);
                                segmento = Pixeles;
                            }
                        }
                        catch { }

                        
                    }


                }
            }



        }

       
    }
}


    

