﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K_Medias
{
    public class Cluster
    {
        public double c0;
        public double c1;
        public double c2;
        public double c3;

        public string ToString()
        {
            return String.Format("({0},{1},{2},{3})", c0, c1, c2, c3);
        }

    }
}
