﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace K_Medias
{
    public partial class Form1 : Form
    {
        Metodos cargar = new Metodos();
        List<double[]> entidades = new List<double[]>();
        List<double[]> clusteres = new List<double[]>();
        List<List<double>> distancias = new List<List<double>>();
        List<List<int>> distanciasComparadas = new List<List<int>>();
        List<double> promedios = new List <double>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            entidades = cargar.leeArchivoEntidades();
            clusteres = cargar.leeArchivoClusteres();
            distancias = cargar.recorrer(clusteres,entidades);
            distanciasComparadas = cargar.comparaDistancias(distancias);
            clusteres = cargar.generarClusteres(clusteres,entidades,distanciasComparadas);
            dataGridView1.DataSource = null;
            mostrarEnDataGrid(clusteres);

            //clusteres = cargar.leeArchivoClusteres();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        public void mostrarEnDataGrid(List <double[]> datos)
        {
            int columnas = datos[0].Length;
            int filas = datos.Count;
            dataGridView1.Rows.Clear();
            foreach(double[] fila in datos)
            {
                DataGridViewRow row = new DataGridViewRow();
                foreach(double dato in fila)
                {
                    DataGridViewCell cell = new DataGridViewTextBoxCell();
                    cell.Value = dato.ToString();
                    row.Cells.Add(cell);
                }
                dataGridView1.Rows.Add(row);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            distancias = cargar.recorrer(clusteres, entidades);
            distanciasComparadas = cargar.comparaDistancias(distancias);
            clusteres = cargar.generarClusteres(clusteres, entidades, distanciasComparadas);
            dataGridView1.DataSource = null;
            mostrarEnDataGrid(clusteres);
        }
    }

}
