﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace K_Medias
{
    class Metodos
    {
        /////////////////////////////////////////////////////////////////////////
        public List <double[]> leeArchivoEntidades()
        {
            string line;
            ///Recorre las lineas del archivo csv para obtener el numero de objetos que iran en el arreglo
            System.IO.StreamReader file = new System.IO.StreamReader(@"C:\Users\52449\source\repos\K-Medias\K-Medias\EntidadesIris2.csv");
            List<string[]> lista = new List<string[]>();
            List<double[]> lista2 = new List<double[]>();
            while ((line = file.ReadLine()) != null)
            {
                string[] datos = line.Split(',');
                lista.Add(datos);
            }
            file.Close();
            lista2=ConvertirArrayDouble(lista);
            return lista2;
        }
        /////////////////////////////////////////////////////////////////////////
        public List <double[]> leeArchivoClusteres ()
        {
            string line;
            List<string[]> lista = new List<string[]>();
            List<double[]> lista2 = new List<double[]>();
            System.IO.StreamReader file = new System.IO.StreamReader(@"C:\Users\52449\source\repos\K-Medias\K-Medias\Clusteresiris.csv");
            List<string> clusterLista = new List<string>();
            while ((line = file.ReadLine()) != null)
            {
                string[] datos = line.Split(',');
                lista.Add(datos);
            }
            file.Close();
            lista2 = ConvertirArrayDouble(lista);
            return lista2; 
        }
        
        /////////////////////////////////////////////////////////////////////////
        
        public List<double[]> ConvertirArrayDouble(List <string[]> lista)
        {
            double aux;
            List<double[]> lista2 = new List<double[]>();
            foreach (string[] fila in lista)
            {
                List<double> lista3 = new List<double>();
                for (int i = 0;i<fila.Length;i++)
                {
                    aux = Convert.ToDouble(fila[i]);
                    lista3.Add(aux);
                    //MessageBox.Show(fila[i]);
                }
                lista2.Add(lista3.ToArray());
            }
            return lista2;
        }

        /////////////////////////////////////////////////////////////////////////
        public List <List<double>> recorrer(List <double[]> lista1, List <double[]> lista2)
        {
            List<List <double>> distanciasLista = new List<List<double>>();
            double aux=0,res;
            int i = 0;
            foreach(double[] cluster in lista1)
            {
                List<double> distancias = new List<double>();
                foreach (double[] entidad in lista2)
                {
                    for(i=0;i<cluster.Length;i++)
                    {
                        res = AplicaFormula(cluster[i], entidad[i],aux);
                        aux = res;
                    }
                    aux = RaizCuadrada(aux);
                    //MessageBox.Show(aux.ToString());
                    distancias.Add(aux);
                }
                distanciasLista.Add(distancias);
            }
            
            return distanciasLista;

        }
        /////////////////////////////////////////////////////////////////////////
        public double AplicaFormula(double a, double b, double aux)
        {
            double cuadrado = new double();
            if (aux == 0)
            {
                double resta;
                resta = a - b;
                cuadrado = Math.Pow(resta,2);
            } else
            {
                double resta;
                resta = a - b;
                cuadrado = Math.Pow(resta,2);
                cuadrado = aux + cuadrado;
            }
            return cuadrado;
        }
        /////////////////////////////////////////////////////////////////////////
        public double RaizCuadrada(double a)
        {
            a = Math.Sqrt(a);
            return a;
        }



        public List <double> promedio (List <double[]> datos, List<int> filasApromediar)
        {
            List<double> resultados = new List<double>();
            int columnas = datos[0].Length;
            int filas = datos.Count;
            for (int indiceColumna = 0; indiceColumna < columnas; indiceColumna++)
            {
                double promColumna=0;
                for (int indiceFilas = 0; indiceFilas < filas; indiceFilas++)
                {
                    if(filasApromediar.Contains(indiceFilas))
                    {
                        promColumna += datos[indiceFilas][indiceColumna];
                    }
                }
                promColumna = promColumna / filasApromediar.Count;
                resultados.Add(promColumna);
            }
                return resultados;
        }
        /////////////////////////////////////////////////////////////////////////
        public List <List <int>> comparaDistancias (List <List <double>> distancias)
        {
            int columnas = distancias[0].Count;
            int filas = distancias.Count;
            List<List<int>> resultado = new List<List<int>>();
            for(int i=0;i<filas;i++)
            {
                resultado.Add(new List<int>());
            }

            for (int indiceColumna = 0; indiceColumna < columnas; indiceColumna++)
            {
                double menor=0;
                int menorClusterIndice = 0;
                for (int indiceFilas = 0; indiceFilas < filas; indiceFilas++)
                {
                    if(indiceColumna == 0)
                    {
                        menor = distancias[indiceFilas][indiceColumna];
                        menorClusterIndice=0;
                    }
                    if(menor < distancias[indiceFilas][indiceColumna])
                    {
                        menor = distancias[indiceFilas][indiceColumna];
                        menorClusterIndice = indiceFilas;
                    }

                }
                resultado[menorClusterIndice].Add(indiceColumna);
            }
            return resultado;
        }
        /////////////////////////////////////////////////////////////////////////
        public List<double[]> generarClusteres(
            List<double[]> originalesCluster,
            List<double[]> originalesDatos,
            List<List<int>> distanciasComparadas)

        {
            List<List<double>> clusterNuevo = new List<List<double>>();
            for (int indiceCluster=0;indiceCluster<distanciasComparadas.Count; indiceCluster++)
            {
                if(distanciasComparadas[indiceCluster].Count == 0)
                {
                    clusterNuevo.Add(new List <double>(originalesCluster[indiceCluster]));
                    continue;
                }
                List<double> valoresPromediados = promedio(originalesDatos, distanciasComparadas[indiceCluster]);
                clusterNuevo.Add(valoresPromediados);
                

            }
            List<double[]> resultado = new List<double[]>();
            foreach(List <double> listaConvercion in clusterNuevo )
            {
                resultado.Add(listaConvercion.ToArray());
            }
                return resultado;


        }
        /////////////////////////////////////////////////////////////////////////


    }

}
